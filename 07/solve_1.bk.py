#!/usr/bin/env python3
## Day 07

files = []
dirs = set()

wd = ""
with open("input.txt", "r") as file:
    for line in file:
        parts = line.strip().split(" ")
        if len(parts) == 3: # $ cd X
            if parts[2] == "/":
                wd = ""
            elif parts[2] == "..":
                wd = "/".join(wd.split("/")[:-1])
                continue
            else:
                wd = wd + "/" + parts[2]
            dirs.add(wd)
        else:
            if parts[0] == "$": continue # $ ls
            if parts[0] == "dir": continue # dir X
            files.append((wd + "/" + parts[1], int(parts[0])))
#print(files)

dir_sizes = []

for dir in dirs:
    dir_sizes.append(
        (
            dir,
            sum([v for x, v in files if x.startswith(dir)])
        )
    )

print(dir_sizes)
print(len(dirs))
print([(k,v) for k, v in dir_sizes if v <= 100000])
print(sum([v for _, v in dir_sizes if v <= 100000]))
# 1919731 is too high
# 1675605 is too low

print(len(set([
    "/".join(x.split("/")[:-1]) for x,_ in files
])))