#!/usr/bin/env python3
## Day 07

files = []
dirs = {}

wd = ""
with open("input.txt", "r") as file:
    for line in file:
        parts = line.strip().split(" ")
        if len(parts) == 3: # $ cd X
            if parts[2] == "/":
                wd = ""
            elif parts[2] == "..":
                wd = "/".join(wd.split("/")[:-1])
            else:
                wd = wd + "/" + parts[2]
        else:
            if parts[0] == "$": continue # $ ls
            if parts[0] == "dir": continue # dir X
            files.append((wd + "/" + parts[1], int(parts[0])))

# print(files)

for path, size in files:
    parts = path.split("/")
    for i in range(1,len(parts)):
        k = "/".join(parts[:i])
        if k == "": k = "/"
        # print("append %d to %s" % (size, k))
        dirs[k] = dirs.setdefault(k, 0) + size

# print(dirs)

# dir_sizes = []

# for dir in dirs:
#     dir_sizes.append(
#         (
#             dir,
#             sum([v for x, v in files if x.startswith(dir)])
#         )
#     )

# print(dir_sizes)
print(len(dirs))
# print([(k,v) for k, v in dirs.items() if v <= 100000])
print(sum([v for _, v in dirs.items() if v <= 100000]))
# 1919731 is too high
# 1675605 is too low
# 1749646

# print(len(set([
#     "/".join(x.split("/")[:-1]) for x,_ in files
# ])))

used = dirs['/']
need = used - 40000000

best = 9999999999999
for path, size in dirs.items():
    if size >= need and size < best:
        best = size

print("Best: %d" % best)
