#!/usr/bin/env python3
from collections import Counter
stop_at = 5

def priority(chr):
    v = ord(chr)
    if v > 96: return v - 96
    return v - 38

cs = 0

a = ""
b = ""

n = 0

with open("input.txt", "r") as file:
    for line in file:
        if n % 3 == 0:
            a  = line.strip()
        elif n % 3 == 1:
            b = line.strip()
        else:
            cs += priority((set(a) & set(b) & set(line)).pop())
        n += 1

print(cs)
