#!/usr/bin/env python3
from collections import Counter
stop_at = 5

def priority(chr):
    v = ord(chr)
    if v > 96: return v - 96
    return v - 38

cs = 0

with open("input.txt", "r") as file:
    for line in file:
        #stop_at -= 1
        #if stop_at < 1: break
        line = line.strip()
        a = set(line[:int(len(line)/2)])
        b = set(line[int(len(line)/2):])
        #print(a & b, priority((a & b).pop()))
        cs += priority((a & b).pop())

print(cs)