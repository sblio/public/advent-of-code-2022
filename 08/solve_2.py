#!/usr/bin/env python3
## Day 08

from math import floor

cols = 99
rows = 99

visible = set()

heights = []
visibility = []

# load
with open("input.txt", "r") as file:
    for row, line in enumerate(file):
        heights.append([
            int(height) for col, height in enumerate(line.strip())
        ])

# print(heights)

def score(row, col):
    height = heights[row][col]
    scr = 0
    scr2 = 1
    # print("Score for %d x %d" % (row, col))
    # north
    for dy in range(row):
        # print("Compare with %d x %d" % (row-dy-1, col))
        if heights[row-dy-1][col] < height:
            scr += 1
        else:
            scr += 1
            break
    scr2 *= scr
    scr = 0
    # east
    for dx in range(cols-col-1):
        # print("Compare with %d x %d" % (row, col+dx+1))
        if heights[row][col+dx+1] < height:
            scr += 1
        else:
            scr += 1
            break
    scr2 *= scr
    scr = 0
    # south
    for dy in range(rows-row-1):
        if heights[row+dy+1][col] < height:
            scr += 1
        else:
            scr += 1
            break
    scr2 *= scr
    scr = 0
    # west
    for dx in range(col):
        if heights[row][col-dx-1] < height:
            scr += 1
        else:
            scr += 1
            break
    scr2 *= scr
    return scr2

for row in range(rows):
    visibility.append([score(row, col) for col in range(cols)])
        
bests = [item for sublist in visibility for item in sublist]
best = max(bests)
pos = bests.index(best)
print("%d at %d x %d" % (best, floor(pos / cols)+1, (pos % cols)+1))