#!/usr/bin/env python3
## Day 08

cols = 99
rows = 99

visible = set()

with open("input.txt", "r") as file:
    tallest_north = [-1]*cols
    for row, line in enumerate(file):
        tallest_west = -1
        for col, height in enumerate(line.strip()):
            height = int(height)
            if height > tallest_north[col]:
                tallest_north[col] = height
                visible.add("%d_%d" % (row, col))
            if height > tallest_west:
                tallest_west = height
                visible.add("%d_%d" % (row, col))

#print(sorted(list(visible)))

with open("input.txt", "r") as file:
    tallest_south = [-1]*cols
    for row, line in enumerate(reversed(file.readlines())):
        tallest_east = -1
        for col, height in enumerate(reversed(line.strip())):
            height = int(height)
            if height > tallest_south[col]:
                tallest_south[col] = height
                visible.add("%d_%d" % (rows-1-row, rows-1-col))
            if height > tallest_east:
                tallest_east = height
                visible.add("%d_%d" % (rows-1-row, rows-1-col))
            
#print(sorted(list(visible)))
print(len(visible))