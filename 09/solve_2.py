#!/usr/bin/env python3
## Day 09

start_x = 2000
start_y = 2000
knots = [
    [start_x, start_y], # H
    [start_x, start_y], # 1
    [start_x, start_y], # 2
    [start_x, start_y], # 3
    [start_x, start_y], # 4
    [start_x, start_y], # 5
    [start_x, start_y], # 6
    [start_x, start_y], # 7
    [start_x, start_y], # 8
    [start_x, start_y]  # 9
]
visited = set()

def update_tail(head, tail):
    # head +x tail
    if head[0] - tail[0] > 1:
        tail[0] += 1
        # +y
        if head[1] - tail[1] > 0:
            tail[1] += 1
        # -y
        if tail[1] - head[1] > 0:
            tail[1] -= 1
    # head -x tail
    if tail[0] - head[0] > 1:
        tail[0] -= 1
        # +y
        if head[1] - tail[1] > 0:
            tail[1] += 1
        # -y
        if tail[1] - head[1] > 0:
            tail[1] -= 1
    # head +y tail
    if head[1] - tail[1] > 1:
        tail[1] += 1
        if head[0] - tail[0] > 0:
            tail[0] += 1
        if tail[0] - head[0] > 0:
            tail[0] -= 1
    # head -y tail
    if tail[1] - head[1] > 1:
        tail[1] -= 1
        if head[0] - tail[0] > 0:
            tail[0] += 1
        if tail[0] - head[0] > 0:
            tail[0] -= 1
    return tail

def draw(head, tail):
    for y in range(1970,2030):
        for x in range(1970,2030):
            if head[0] == (x) and head[1] == (y):
                print("H", end = "")
            elif tail[0] == (x) and tail[1] == (y):
                print("T", end = "")
            elif ("%s_%s" % (x, y)) in visited:
                print("#", end = "")
            else:
                print(".", end = "")
        print("")
    print("")

with open("input.txt", "r") as file:
    for line in file:
        dir, dist = line.strip().split(" ")
        dist = int(dist)
        if dir == "R":
            for _ in range(dist):
                knots[0][0] += 1
                for k in range(9):
                    knots[k+1] = update_tail(knots[k], knots[k+1])
                visited.add("%d_%d" % (knots[9][0], knots[9][1]))
                # draw(head, tail)
        elif dir == "U":
            for _ in range(dist):
                knots[0][1] += 1
                for k in range(9):
                    knots[k+1] = update_tail(knots[k], knots[k+1])
                visited.add("%d_%d" % (knots[9][0], knots[9][1]))
                # draw(head, tail)
        elif dir == "L":
            for _ in range(dist):
                knots[0][0] -= 1
                for k in range(9):
                    knots[k+1] = update_tail(knots[k], knots[k+1])
                visited.add("%d_%d" % (knots[9][0], knots[9][1]))
                # draw(head, tail)
        else:
            for _ in range(dist):
                knots[0][1] -= 1
                for k in range(9):
                    knots[k+1] = update_tail(knots[k], knots[k+1])
                visited.add("%d_%d" % (knots[9][0], knots[9][1]))
                # draw(head, tail)

# print(sorted(list(visited)))
draw(knots[0], knots[9])
print(len(visited))