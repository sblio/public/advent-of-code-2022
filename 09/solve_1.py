#!/usr/bin/env python3
## Day 09

start_x = 2000
start_y = 2000
head = [start_x, start_y]
tail = [start_x, start_y]
visited = set()

def update_tail(head, tail):
    # head +x tail
    if head[0] - tail[0] > 1:
        tail[0] += 1
        # +y
        if head[1] - tail[1] > 0:
            tail[1] += 1
        # -y
        if tail[1] - head[1] > 0:
            tail[1] -= 1
    # head -x tail
    if tail[0] - head[0] > 1:
        tail[0] -= 1
        # +y
        if head[1] - tail[1] > 0:
            tail[1] += 1
        # -y
        if tail[1] - head[1] > 0:
            tail[1] -= 1
    # head +y tail
    if head[1] - tail[1] > 1:
        tail[1] += 1
        if head[0] - tail[0] > 0:
            tail[0] += 1
        if tail[0] - head[0] > 0:
            tail[0] -= 1
    # head -y tail
    if tail[1] - head[1] > 1:
        tail[1] -= 1
        if head[0] - tail[0] > 0:
            tail[0] += 1
        if tail[0] - head[0] > 0:
            tail[0] -= 1
    return tail

def draw(head, tail):
    for y in range(2000,2006):
        for x in range(2000,2006):
            if head[0] == (x) and head[1] == (y):
                print("H", end = "")
            elif tail[0] == (x) and tail[1] == (y):
                print("T", end = "")
            elif ("%s_%s" % (x, y)) in visited:
                print("#", end = "")
            else:
                print(".", end = "")
        print("")
    print("")

with open("input.txt", "r") as file:
    for line in file:
        dir, dist = line.strip().split(" ")
        dist = int(dist)
        if dir == "R":
            for _ in range(dist):
                head[0] += 1
                tail = update_tail(head, tail)
                visited.add("%d_%d" % (tail[0], tail[1]))
                # draw(head, tail)
        elif dir == "U":
            for _ in range(dist):
                head[1] += 1
                tail = update_tail(head, tail)
                visited.add("%d_%d" % (tail[0], tail[1]))
                # draw(head, tail)
        elif dir == "L":
            for _ in range(dist):
                head[0] -= 1
                tail = update_tail(head, tail)
                visited.add("%d_%d" % (tail[0], tail[1]))
                # draw(head, tail)
        else:
            for _ in range(dist):
                head[1] -= 1
                tail = update_tail(head, tail)
                visited.add("%d_%d" % (tail[0], tail[1]))
                # draw(head, tail)

# print(sorted(list(visited)))
print(len(visited))