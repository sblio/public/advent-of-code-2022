#!/usr/bin/env python3
# Day 12 - part 1

grid = []
score = []
start = [0, 0]
end = [0, 0]

with open("input.txt", "r") as file:
    for row, line in enumerate(file):
        grid.append([ord(v) for v in line.strip()])
        score.append([9999 for _ in line.strip()])
        for col, char in enumerate(line.strip()):
            if char == "S":
                grid[row][col] = ord("a")
                start = [row, col]
                score[row][col] = 0
            if char == "E":
                grid[row][col] = ord("z")
                end = [row, col]

def draw(grid):
    for row in grid:
        for cell in row:
            print(" %4d" % cell, end = "")
        print()

draw(grid)
print(start)
print(end)

width = len(grid[0])
height = len(grid)

def walk(row, col, md):
    md -= 1
    if md < 0: return
    curr = score[row][col]
    hght = grid[row][col]
    north = False
    south = False
    east = False
    west = False

    if row == end[0] and col == end[1]:
        print("ENDDD: %d" % curr)
        return True

    if row > 0 and grid[row-1][col] < hght + 2:
        if curr < score[row-1][col] + 1:
            score[row-1][col] = curr + 1
            north = True

    if row < height - 1 and grid[row+1][col] < hght + 2:
        if curr < score[row+1][col] + 1:
            score[row+1][col] = curr + 1
            south = True

    if col > 0 and grid[row][col-1] < hght + 2:
        if curr < score[row][col-1] + 1:
            score[row][col-1] = curr + 1
            east = True

    if col < width - 1 and grid[row][col+1] < hght + 2:
        if curr < score[row][col+1] + 1:
            score[row][col+1] = curr + 1
            west = True
    
    if north:
        ended = walk(row-1, col, md)
        if ended: return True
    if south:
        ended = walk(row+1, col, md)
        if ended: return True
    if east:
        ended = walk(row, col-1, md)
        if ended: return True
    if west:
        ended = walk(row, col+1, md)
        if ended: return True

    return False

walk(start[0], start[1], 2000)
draw(score)