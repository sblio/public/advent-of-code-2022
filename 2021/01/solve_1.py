#!/usr/bin/env python3
# Day 01 - part 1

increases = 0

with open("input.txt", "r") as file:
    last = 9999999
    for line in file:
        depth = int(line.strip())
        if depth > last:
            increases += 1
        last = depth

print(increases)
