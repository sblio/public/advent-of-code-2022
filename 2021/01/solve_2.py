#!/usr/bin/env python3
# Day 01 - part 1

values = []
increases = 0

with open("input.txt", "r") as file:
    for line in file:
        values.append(int(line.strip()))

last = 9999999
for i in range(len(values)-2):
    val = sum(values[i:i+3])
    # print(val)
    if val > last: increases += 1
    last = val

# print(values)
print(increases)
