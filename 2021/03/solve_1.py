#!/usr/bin/env python3
# Day 03 - part 1

bits = [0] * 5
length = 12
fn = "example.txt"

bits = [0] * 12
length = 1000
fn = "input.txt"

with open(fn, "r") as file:
    for line in file:
        newbits = line.strip()
        for i, bit in enumerate(newbits):
            bits[i] += int(bit)

print(bits)
print([round(b / length) for b in bits])
print(int("".join([str(round(b / length)) for b in bits]), 2))
# 784 is too low
