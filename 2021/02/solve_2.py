#!/usr/bin/env python3
# Day 02 - part 1

depth = 0
dist = 0
aim = 0

with open("input.txt", "r") as file:
    for line in file:
        dir, v = line.strip().split(" ")
        if dir == "forward":
            dist += int(v)
            depth += aim * int(v)
        elif dir == "up":
            aim -= int(v)
        elif dir == "down":
            aim += int(v)

print(depth, dist, depth * dist)
