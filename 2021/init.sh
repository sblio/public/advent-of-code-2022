#!/bin/bash

year=2021
# "https://adventofcode.com/${year}/day/1/input"

for day in $(seq -w 1 25); do
    mkdir -p ${day}
    touch "${day}/input.txt"
    touch "${day}/example.txt"
    cat >"${day}/solve_1.py" <<EOF
#!/usr/bin/env python3
# Day ${day} - part 1

data = []

with open("example.txt", "r") as file:
    for line in file:
        data.append(line.strip().split(" "))

print(data)
EOF
    chmod +x "${day}/solve_1.py"
    # day_short=$(($day))
    # wget -O "${day}/input.txt" "https://adventofcode.com/${year}/day/${day_short}/input"
done
