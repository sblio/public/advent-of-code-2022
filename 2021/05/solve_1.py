#!/usr/bin/env python3
# Day 05 - part 1

data = []

with open("example.txt", "r") as file:
    for line in file:
        data.append(line.strip().split(" "))

print(data)
