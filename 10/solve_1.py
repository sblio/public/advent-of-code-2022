#!/usr/bin/env python3
## Day 10

x = [1]

with open("input.txt", "r") as file:
    for line in file:
        op = line.strip().split(" ")
        if op[0] == "noop":
            x.append(x[-1])
        else:
            x.append(x[-1])
            x.append(x[-1] + int(op[1]))

print(x)
print(
    x[20-1] * 20,
    x[60-1] * 60,
    x[100-1] * 100,
    x[140-1] * 140,
    x[180-1] * 180,
    x[220-1] * 220
)
print(sum([
    x[20-1] * 20,
    x[60-1] * 60,
    x[100-1] * 100,
    x[140-1] * 140,
    x[180-1] * 180,
    x[220-1] * 220
]))