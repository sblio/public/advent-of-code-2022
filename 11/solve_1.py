#!/usr/bin/env python3
# Day 11 - part 1

data = []
monkeys = []

with open("input.txt", "r") as file:
    monkey = {
        "items": [],
        "operator": "",
        "modifier": "",
        "divisor": 0,
        "true": 0,
        "false": 0,
        "activity": 0,
    }
    for line in file:
        parts = line.strip().split(" ")
        if parts[0] == "Starting":
            monkey["items"] = [int(x) for x in "".join(parts[2:]).split(",")]
        elif parts[0] == "Operation:":
            monkey["operator"] = parts[4]
            monkey["modifier"] = parts[5]
        elif parts[0] == "Test:":
            monkey["divisor"] = int(parts[3])
        elif parts[0] == "If" and parts[1] == "true:":
            monkey["true"] = int(parts[5])
        elif parts[0] == "If" and parts[1] == "false:":
            monkey["false"] = int(parts[5])
        elif parts[0] == "":
            monkeys.append(monkey.copy())
            monkey = {
                "items": [],
                "operator": "",
                "modifier": "",
                "divisor": 0,
                "true": 0,
                "false": 0,
                "activity": 0,
            }

print(monkeys)

def update_worry(old, op, by):
    if by == "old": by = old
    if op == "+":
        return int( (old + int(by)) / 3 )
    if op == "*":
        return int( (old * int(by)) / 3 )

for _ in range(20):
    for monkey in monkeys:
        for worry in monkey["items"]:
            worry = update_worry(
                worry, monkey["operator"], monkey["modifier"])
            monkey["activity"] += 1
            if worry % monkey["divisor"] == 0:
                monkeys[monkey["true"]]["items"].append(worry)
            else:
                monkeys[monkey["false"]]["items"].append(worry)
        monkey["items"] = []

print(monkeys)
print([m["activity"] for m in monkeys])