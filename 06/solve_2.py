#!/usr/bin/env python3
## Day 06

with open("input.txt", "r") as file:
    buff = file.readline()
    for i in range(len(buff) - 14):
        # print(buff[i:i+14])
        if len(set(buff[i:i+14])) == 14:
            print(i+14)
            print(buff[i:i+14])
            break