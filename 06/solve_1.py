#!/usr/bin/env python3
## Day 06

a, b, c, d = "", "", "", ""

with open("input.txt", "r") as file:
    buff = file.readline()
    a, b, c, d = buff[:4]
    if len(set((a,b,c,d))) == 4: print(4)
    else:
        for i, char in enumerate(buff[4:]):
            a, b, c, d = b, c, d, char
            if len(set((a,b,c,d))) == 4:
                print(i+4+1)
                break

print(a, b, c, d)