#!/usr/bin/env python3

max_initial_height = 8 # first blank line - 2
n_initial_stacks = 9 # (length of first line + 1) / 4

stacks = [
    [], [], [], [], [], [], [], [], []
]

state = 0

with open("input.txt", "r") as file:
    for line in file:
        if state > 0:
            _, n, _, a, _, b = line.strip().split(" ")
            #print("%s => %s (x%s)" % (a,b,n))
            pick = stacks[int(a)-1][-int(n):]
            pick.reverse()
            stacks[int(b)-1] = stacks[int(b)-1] + pick
            stacks[int(a)-1] = stacks[int(a)-1][:-int(n)]

        elif line.strip() == "":
            state = 1
            for i, v in enumerate(stacks):
                stacks[i].pop() # drop the stack number
                stacks[i].reverse()
        else:
            crates = [line[v*4+1] for v in range(9)]
            print(crates)
            for i, v in enumerate(crates):
                if v != " ": stacks[i].append(v)

print(stacks)

for stack in stacks:
    print(stack.pop())