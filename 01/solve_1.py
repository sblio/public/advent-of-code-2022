#!/usr/bin/env python3

current_total = 0
current_r = 1
largest_r = 0
largest_total = 0
with open("input.txt", "r") as file:
    for line in file:
        line = line.strip()
        if line == "":
            if current_total > largest_total:
                largest_total = current_total
                largest_r = current_r
            current_r += 1
            current_total = 0
        else:
            current_total += int(line)

print(largest_r)
print(largest_total)