#!/usr/bin/env python3

largest_totals = [0,0,0]
def update(t, v):
    return [x if x > v else v for x in t]


current_total = 0
with open("input.txt", "r") as file:
    for line in file:
        line = line.strip()
        if line == "":
            print(current_total)
            print(largest_totals)
            for i,v in enumerate(largest_totals):
                if current_total > v:
                    largest_totals[i] = current_total
                    print("updated")
                    break
            largest_totals = sorted(largest_totals)
            #larget_totals = [x if x > current_total else current_total for x in largest_totals]
            current_total = 0
        else:
            # print("increment")
            current_total += int(line)

print(largest_totals)
print(sum(largest_totals))