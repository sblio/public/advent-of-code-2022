#!/usr/bin/env python3

bad = 0

with open("input.txt", "r") as file:
    for line in file:
        p1, p2 = line.strip().split(",")
        p1a, p1b = p1.split("-")
        p2a, p2b = p2.split("-")
        p1 = set(range(int(p1a), int(p1b)+1))
        p2 = set(range(int(p2a), int(p2b)+1))

        if len(p1 & p2) > 0:
            bad += 1

print(bad)
