#!/usr/bin/env python3

values = {
    'A': 1,
    'B': 2,
    'C': 3
}

losing = {
    1: 3,
    2: 1,
    3: 2
}

winning = {
    1: 2,
    2: 3,
    3: 1
}

def mymove(a, x):
    if x == "Y":
        return a
    if x == "X":
        return losing[a]
    if x == "Z":
        return winning[a]

def score(a, b):
    x = values[a]
    y = mymove(x, b)
    if b == "Y":
        x += 3
        y += 3
    if b == "X":
        x += 6
    if b == "Z":
        y += 6
    return (x, y)

your_score = 0
my_score = 0
with open("input.txt", "r") as file:
    for line in file:
        a, b = line.strip().split(" ")
        x, y = score(a, b)
        your_score += x
        my_score += y

print("====")
print(your_score)
print(my_score)