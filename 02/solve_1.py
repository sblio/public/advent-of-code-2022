#!/usr/bin/env python3

values = {
    'A': 1,
    'B': 2,
    'C': 3,
    'X': 1,
    'Y': 2,
    'Z': 3
}

def winnerB(a, b):
    if a == 3 and b == 1:
        return True
    if a == 1 and b == 3:
        return False
    return b > a

def score(a, b):
    x = values[a]
    y = values[b]
    if x == y:
        x += 3
        y += 3
    elif winnerB(x, y):
        y += 6
    else:
        x += 6
    return (x, y)

your_score = 0
my_score = 0
with open("input.txt", "r") as file:
    for line in file:
        a, b = line.strip().split(" ")
        x, y = score(a, b)
        your_score += x
        my_score += y

print("====")
print(your_score)
print(my_score)